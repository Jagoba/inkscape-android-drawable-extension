#!/usr/bin/env python
"""
androidexport.py

Copyright (C) 2012 Akting Ingeniaritza, info@akting.eu

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import os
import errno
import tempfile
import inkex

class Export(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option("-d", "--projectfolder",
                        action="store", type="string",
                        dest="projectfolder", default=os.getenv('HOME'),
                        help="Project folder")
        self.OptionParser.add_option("-f", "--filename",
                        action="store", type="string",
                        dest="filename", default="ic_launcher.png",
                        help="Filename")

    def export(self, output_versions):
    	output_dir = '%s/res' % self.options.projectfolder

    	fd, tmp = tempfile.mkstemp(".svg", text=True)

    	try:
    		self.document.write(tmp)

    		for version in output_versions:
    			try:
    				os.makedirs('%s/%s' % (output_dir, version['name']))
    			except OSError as e:
    				if e.errno != errno.EEXIST:
    					raise

    			output_file = os.path.abspath(os.path.join(output_dir, version['name'], self.options.filename))
    			inkscape_str = 'inkscape %s --export-png=%s --export-width=%d --export-height=%d' % (tmp, output_file, version['width'], version['height'])
    			try:
    				from subprocess import Popen, PIPE
    				p = Popen(inkscape_str, shell=True, stdout=PIPE, stderr=PIPE)
    				rc = p.wait()
    				out = p.stdout.read()
    				err = p.stderr.read()
    			except ImportError:
    				from popen2 import Popen3
    				p = Popen3(inkscape_str, True)
    				p.wait()
    				rc = p.poll()
    				out = p.fromchild.read()
    				err = p.childerr.read()
    		self.parse(tmp)
    		self.getposinlayer()
    		self.getselected()
    		self.getdocids()
    	finally:
    		os.close(fd)
    		os.remove(tmp)
