#!/usr/bin/env python
"""
android_export-drawables-image.py

Copyright (C) 2014 Akting Ingeniaritza, info@akting.eu

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import androidexport

class Image(androidexport.Export):
    def __init__(self):
        androidexport.Export.__init__(self)
        self.OptionParser.add_option("-a", "--baseheight",
                        action="store", type="int",
                        dest="baseheight", default="120",
                        help="baseheight")
        self.OptionParser.add_option("-w", "--basewidth",
                        action="store", type="int",
                        dest="basewidth", default="120",
                        help="basewidth")

    def effect(self):
        base_icon_width = self.options.basewidth
        base_icon_height = self.options.baseheight
        output_versions = [
            {'name': 'drawable-ldpi', 'width': base_icon_width*.75, 'height': base_icon_height*.75},
            {'name': 'drawable-mdpi', 'width': base_icon_width, 'height': base_icon_height},
            {'name': 'drawable-hdpi', 'width': base_icon_width*1.5, 'height': base_icon_height*1.5},
            {'name': 'drawable-xhdpi', 'width': base_icon_width*2, 'height': base_icon_height*2},
            {'name': 'drawable-xxhdpi', 'width': base_icon_width*3, 'height': base_icon_height*3},
            {'name': 'drawable-xxxhdpi', 'width': base_icon_width*4, 'height': base_icon_height*4},
        ]

        self.export(output_versions)

if __name__ == '__main__':
    image = Image()
    image.affect()
